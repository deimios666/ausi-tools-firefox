(async () => {

    //MARK:===REMOVE IN PROD 
    await import("https://cdn.sheetjs.com/xlsx-0.20.2/package/dist/xlsx.mini.min.js");
    const head = document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    link.id = "bootstrapCss";
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = "https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css";
    link.media = "all";
    head.appendChild(link);
    //MARK:/===REMOVE IN PROD

    //MARK: CONFIG
    const baseUrl = "https://transfer.rocnee.eu/ausi";
    const mainNodeUrl = `${baseUrl}/ilmapp/ajaxloadmain`;
    const subNodeUrl = `${baseUrl}/Ilmapp/ajax`;
    const leafListUrl = `${baseUrl}/ilmapp/ajaxedit`;
    const leafEditUrl = `${baseUrl}/ilmapp/ajaxupdate`;
    const siiirCodeEditUrl =
        `${baseUrl}/Ilmapp/validateschool`; //note the big I
    const mainNodeParam =
        "operation=main&businessCategorySearch=undefined&sn=undefined&nameSearch=undefined&dn=undefined&s_tel=undefined";
    const stRegex = />(st=[^<]*)/;
    const localityRegex = />(l=[^<]*)/g;
    const ouRegex = />(ou=[^<]*)/g;
    const ouSiiirCodeRegex = /cod_siiir" value="([0-9]*)/;
    const ouSiiirNameRegex = /Denumire: <\/span>([^<]*)</;
    const ouCentruRegex = /selected >([^<]*)/;
    const cnRegex = />(cn=[^<]*)/g;
    const sNameRegex = /id="sname" value="([^"]*)/;
    const telNrRegex = /id="telephoneNumber" value="([0-9]*)/;
    const mailRegex = /id="mail" value="([^"]*)/;
    const cnpRegex = /id="cnp" value="([0-9]*)/;
    const serienrRegex = /id="serienr" value="([^"]*)/;
    const domiciliuRegex = /id="domiciliu"[^>]*>(.*)<\/textarea>/;
    const di0 = /value="bacActiv" CHECKED>/;
    const di2 = /value="bacMatern" CHECKED>/;
    const di3 = /value="bacSpecial" CHECKED>/;
    const po0 = /value="capacitateActiv" CHECKED>/;
    const po2 = /value="capacitateMatern" CHECKED>/;
    const po3 = /value="capacitateSpecial" CHECKED>/;
    const csvHeader = [
        "ID",
        "Cod SIIIR",
        "Unitate SIIIR",
        "Centru",
        "Nume",
        "Telefon",
        "Email",
        "CNP",
        "CI",
        "Domiciliu",
        "bacActiv",
        "bacMatern",
        "bacSpecial",
        "en8Activ",
        "en8Matern",
        "en8Special"
    ];
    const csvData = [];
    csvData.push(csvHeader);

    //MARK: HELPER FUNCTIONS
    const getNode = async (param) => {
        const response = await fetch(mainNodeUrl, {
            method: "post",
            headers: {
                "Accept": "text/html, */*",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: param
        });
        return await response.text();
    }

    const getSubNode = async (param) => {
        const response = await fetch(subNodeUrl, {
            method: "post",
            headers: {
                "Accept": "text/html, */*",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "X-Requested-With": "XMLHttpRequest"
            },
            body: param
        });
        return await response.text();
    }

    const getLeafList = async (param) => {
        const response = await fetch(leafListUrl, {
            method: "post",
            headers: {
                "Accept": "text/html, */*",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "X-Requested-With": "XMLHttpRequest"
            },
            body: param
        });
        return await response.text();
    }

    const displayMessage = (message) => {
        displayMessageWithSeverity(message, "primary");
    }

    const displayError = (message) => {
        displayMessageWithSeverity(message, "danger");
    }

    const displayWarning = (message) => {
        displayMessageWithSeverity(message, "warning");
    }

    const displaySuccess = (message) => {
        displayMessageWithSeverity(message, "success");
    }

    const displayMessageWithSeverity = (message, severity) => {
        const messageDiv = document.getElementById("message");
        messageDiv.className = `alert alert-${severity}`;
        messageDiv.innerText = message;
    }

    const is = (variable) => {
        return !(typeof variable === 'undefined');
    }

    const makeParamInternal = (prefix, st, l, ou, cn) => {
        let param = prefix;
        if (cn) param += `${cn},`;
        if (ou) param += `${ou},`;
        if (l) param += `${l},`;
        if (st) param += st;
        param += "&del=&ult=_ult";
        return param;
    }

    const makeParam = (st, l, ou, cn) => makeParamInternal("dn=", st, l, ou, cn);
    const makeDetailParam = (st, l, ou, cn) => makeParamInternal("edit=", st, l, ou, cn);



    const getReg = (data, regex) => {
        let retVal = "";
        let match = regex.exec(data);
        if (match != null) {
            retVal = match[1];
        }
        return retVal;
    }

    const getCReg = (data, regex) => {
        let retVal = "";
        let match = regex.exec(data);
        if (match != null) {
            retVal = "DA";
        }
        return retVal;
    }

    //MARK: SETBUSY
    const setBusy = (busy) => {
        if (busy) {
            document.body.style.cursor = "wait";
            const anchors = document.getElementsByTagName("a");
            for (const anchor of anchors) {
                //disable all links
                anchor.style.pointerEvents = "none";
            }
            //show progress bar
            document.getElementById("progressOuter").style.display = "flex";
        } else {
            document.body.style.cursor = "default";
            const anchors = document.getElementsByTagName("a");
            for (const anchor of anchors) {
                //enable all links
                anchor.style.pointerEvents = "auto";
            }
            //hide progress bar
            document.getElementById("progressOuter").style.display = "none";
        }
    }

    //MARK: PROGRESS UPDATE
    const updateProgress = (progress) => {
        const progressBarOuterDiv = document.getElementById("progressOuter");
        const progressBarInner = document.getElementById("progressInner");

        progressBarOuterDiv.ariaValueNow = `${progress.toFixed(0)}%`;
        progressBarInner.style.width = `${progress.toFixed(0)}%`;
        progressBarInner.textContent = `${progress.toFixed(0)}%`;
        //console.log(progress);
    };

    //MARK: EXPORT
    const doExport = async () => {
        setBusy(true);
        displayMessage("Export in curs de desfășurare");
        const mainNodeHtml = await getNode(mainNodeParam);
        const stArray = stRegex.exec(mainNodeHtml);
        const stValue = stArray[1];
        const localityParam = makeParam(stValue);

        const localityHtml = await getSubNode(localityParam);
        const localityArray = localityHtml.match(localityRegex).map((x) => x.substring(1));
        //set loading indicator
        let recordCounter = 0;
        let timer = setInterval(() => {
            updateProgress(recordCounter / totalRecords * 100);
        }, 1000);
        let totalRecords = localityArray.length;
        //get organizational units (ou)
        for (const currentLocality of localityArray) {
            const ouHtml = await getSubNode(makeParam(stValue, currentLocality));
            const ouArray = ouHtml.match(ouRegex).map((x) => x.substring(1));
            totalRecords += ouArray.length;
            for (const currentOu of ouArray) {
                const ouDetailResponseText = await getLeafList(makeDetailParam(stValue, currentLocality, currentOu));
                const currentCentru = getReg(ouDetailResponseText, ouCentruRegex);
                const currentSiiirCode = getReg(ouDetailResponseText, ouSiiirCodeRegex);
                const currentSiiirName = getReg(ouDetailResponseText, ouSiiirNameRegex);
                const cnHtml = await getSubNode(makeParam(stValue, currentLocality, currentOu));
                const cnMatch = cnHtml.match(cnRegex);
                if (!cnMatch) {
                    continue;
                }
                const cnArray = cnMatch.map((x) => x.substring(1));
                for (const currentCn of cnArray) {
                    const cnDetailResponseText = await getLeafList(makeDetailParam(stValue, currentLocality, currentOu, currentCn));
                    const id = `${currentCn},${currentOu},${currentLocality},${stValue}`;
                    console.log(`[${recordCounter}/${totalRecords}]=${id}`);
                    let oneLine = [];
                    oneLine[0] = id;
                    oneLine[1] = currentSiiirCode;
                    oneLine[2] = currentSiiirName;
                    oneLine[3] = currentCentru;
                    oneLine[4] = getReg(cnDetailResponseText, sNameRegex);
                    oneLine[5] = getReg(cnDetailResponseText, telNrRegex);
                    oneLine[6] = getReg(cnDetailResponseText, mailRegex);
                    oneLine[7] = getReg(cnDetailResponseText, cnpRegex);
                    oneLine[8] = getReg(cnDetailResponseText, serienrRegex);
                    oneLine[9] = getReg(cnDetailResponseText, domiciliuRegex);
                    oneLine[10] = getCReg(cnDetailResponseText, di0);
                    oneLine[11] = getCReg(cnDetailResponseText, di2);
                    oneLine[12] = getCReg(cnDetailResponseText, di3);
                    oneLine[13] = getCReg(cnDetailResponseText, po0);
                    oneLine[14] = getCReg(cnDetailResponseText, po2);
                    oneLine[15] = getCReg(cnDetailResponseText, po3);
                    csvData.push(oneLine);
                }
                recordCounter++;
            }
            recordCounter++;
        }
        clearInterval(timer);
        setBusy(false);

        //save xlsx
        let worksheet = XLSX.utils.aoa_to_sheet(csvData);
        let workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");
        XLSX.writeFile(workbook, "export_ausi.xlsx");
        displaySuccess("Export finalizat");
    }

    //MARK: IMPORT
    const doImport = (event) => {
        let doSIIIRSync = false;
        const siiirSyncCheckbox = document.getElementById("siiirSync");
        if (siiirSyncCheckbox.checked) {
            doSIIIRSync = true;
        }
        setBusy(true);
        displayMessage("Import in curs de desfășurare");
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = async (e) => {
            const data = new Uint8Array(e.target.result);
            const workbook = XLSX.read(data, {
                type: "array"
            });
            let importArray = XLSX.utils.sheet_to_json(workbook.Sheets["Sheet1"], {
                header: 1
            });
            let progressCounter = 0;
            let errorcounter = 0;
            let totalRecords = importArray.length;
            let timer = setInterval(() => {
                updateProgress(Math.round((progressCounter / totalRecords * 100)));
            }, 1000);

            let errorLines = [];

            for (const [lineIndex, importItem] of importArray.entries()) {
                if (importItem[0].substr(0, 3) == "cn=") {
                    let items = importItem;
                    let di = "";
                    let po = "";

                    if (is(items[10]) && items[10].length > 0) {
                        di = "10";
                    } else {
                        di = "00";
                    }

                    if (is(items[11]) && items[11].length > 0) {
                        di = di + "1";
                    } else {
                        di = di + "0";
                    }

                    if (is(items[12]) && items[12].length > 0) {
                        di = di + "1";
                    } else {
                        di = di + "0";
                    }

                    if (is(items[13]) && items[13].length > 0) {
                        po = "10";
                    } else {
                        po = "00";
                    }

                    if (is(items[14]) && items[14].length > 0) {
                        po = po + "1";
                    } else {
                        po = po + "0";
                    }

                    if (is(items[15]) && items[15].length > 0) {
                        po = po + "1";
                    } else {
                        po = po + "0";
                    }

                    let params = {
                        //operation: "contact",
                        edit: items[0],
                        sn: items[4],
                        givenName: "",
                        telephoneNumber: items[5],
                        mail: items[6],
                        cnp: items[7],
                        serienr: items[8],
                        domiciliu: items[9],
                        destinationIndicator: di,
                        postOfficeBox: po
                    };

                    const formBody = Object.keys(params).reduce((acc, key) => {
                        return acc + "&" + encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
                    }, "operation=contact");

                    try {
                        await fetch(leafEditUrl, {
                            method: "post",
                            credentials: "same-origin",
                            headers: {
                                Accept: "text/html, */*",
                                "Content-type":
                                    "application/x-www-form-urlencoded; charset=UTF-8",
                                "X-Requested-With": "XMLHttpRequest"
                            },
                            body: formBody
                        });
                        //const updateResponseText = await updateResponse.text();

                        const ouArray = items[0].split(',');
                        ouArray.splice(0, 1);
                        const ou = ouArray.join(',');

                        let businessCategory = "null";
                        if (items[3] == "Centru de comunicare") {
                            businessCategory = "businessCategory";
                        }

                        //update centru comunicare
                        if (doSIIIRSync) {
                            await fetch(leafEditUrl, {
                                method: "post",
                                credentials: "same-origin",
                                headers: {
                                    Accept: "text/html, */*",
                                    "Content-type":
                                        "application/x-www-form-urlencoded; charset=UTF-8",
                                    "X-Requested-With": "XMLHttpRequest"
                                },
                                body:
                                    "operation=scoala&edit=" +
                                    encodeURIComponent(ou) +
                                    `&businessCategory=cn%3D${businessCategory}%2Cc%3Dro`
                            });

                            const siiir_code = items[1];

                            if ((siiir_code + "").match(/^\d+$/)) {
                                //update cod SIIIR
                                await fetch(siiirCodeEditUrl, {
                                    method: "post",
                                    credentials: "same-origin",
                                    headers: {
                                        Accept: "text/html, */*",
                                        "Content-type":
                                            "application/x-www-form-urlencoded; charset=UTF-8",
                                        "X-Requested-With": "XMLHttpRequest"
                                    },
                                    body: "dn=" + encodeURIComponent(ou) + "&siiir_code=" + siiir_code
                                });
                            }
                        }
                        progressCounter++;
                    } catch (err) {
                        errorLines.push(lineIndex + 1);
                        errorcounter++;
                        progressCounter++;
                    }
                } else {
                    //we only mark error if we are not on the first line
                    if (lineIndex > 0) {
                        errorLines.push(lineIndex + 1);
                        errorcounter++;
                    }
                    progressCounter++;
                }
            }
            setBusy(false);
            clearInterval(timer);
            event.target.value = null;
            if (errorcounter == 0) {
                displaySuccess("Import finalizat cu succes");
            } else {
                displayWarning(`Import finalizat cu erori. Liniile cu erori: [${errorLines.join(",")}]`);
            }

        }
        reader.readAsArrayBuffer(file);
    }

    const patchCSS = (selectorText, styleDeclaration, styleValue) => {
        const CSSStyleSheets = document.styleSheets;
        for (const CSSStyleSheet of CSSStyleSheets) {
            try {
                const cssRules = CSSStyleSheet.cssRules;
                for (const CSSStyleRule of cssRules) {

                    if (CSSStyleRule.selectorText.trim() == selectorText.trim()) {
                        // console.log(`MATCH: ${CSSStyleRule.selectorText.trim()}`);
                        // console.log(CSSStyleRule.style);
                        // console.log(`Setting ${styleDeclaration} from ${CSSStyleRule.style[styleDeclaration]} to ${styleValue}`);
                        CSSStyleRule.style[styleDeclaration] = styleValue;
                        //console.log(`New value: ${CSSStyleRule.style[styleDeclaration]}`);
                    }
                }
            } catch (err) {
                //console.log(err);

            }
        }
    }

    const addCSS = (css) => {
        const sheet = document.styleSheets[0];
        sheet.insertRule(css, sheet.cssRules.length);
    }

    //MARK: SETUP
    const doSetup = async () => {
        const contentDiv = document.getElementById("content");
        contentDiv.style.width = "auto";

        //MARK: CSS PATCHES

        patchCSS("body, html", "background-color", "white");

        patchCSS(".link_meniu", "width", "auto");
        patchCSS(".link_meniu", "display", "flex");
        patchCSS(".link_meniu", "align-items", "center");
        patchCSS(".link_meniu", "justify-content", "center");
        patchCSS(".link_meniu", "height", "1.6rem");
        patchCSS(".link_meniu", "vertical-align", "middle");

        patchCSS("div.extern_ovf", "height", "auto");
        patchCSS("div.extern_ovf", "max-width", "700px");
        patchCSS("div.extern_ovf", "min-height", "600px");
        patchCSS("div.extern_ovf", "position", "relative");
        patchCSS("div.extern_ovf", "top", "1rem");
        patchCSS("div.extern_ovf", "border", "1px solid lightgrey");
        patchCSS("div.extern_ovf", "border-radius", "5px");
        patchCSS("div.extern_ovf", "background-color", "transparent");
        patchCSS("div.extern_ovf", "transition", "height .3s ease");

        patchCSS("div.search", "position", "relative");
        patchCSS("div.search", "top", "auto");
        patchCSS("div.search", "border", "1px solid lightgrey");
        patchCSS("div.search", "border-radius", "5px");

        patchCSS("div.edit", "border", "1px solid lightgrey");
        patchCSS("div.edit", "border-radius", "5px");
        patchCSS("div.edit", "width", "auto");
        patchCSS("div.edit", "padding-right", "1rem");

        patchCSS("div.extern", "max-height", "none"); //why the f is auto not a valid value?
        patchCSS("div.extern", "width", "auto");
        patchCSS("div.extern", "border", "0");

        //MARK: REMOVE OLD STUFF
        //remove old links
        const oldLinks = document.getElementsByClassName("link_meniu");
        for (const oldLink of Array.from(oldLinks)) {
            oldLink.parentElement.removeChild(oldLink);
        }

        //remove old menu
        const oldMenu = document.getElementById("div_meniu");
        if (oldMenu) {
            oldMenu.parentElement.removeChild(oldMenu);
        }

        //remove width of all H1
        const h1 = document.getElementsByTagName("h1");
        for (const h of Array.from(h1)) {
            h.style.width = "auto";
        }

        //MARK: LAYOUT
        const divContent = document.getElementById("content");
        divContent.style.display = "flex";
        divContent.style.flexDirection = "column";

        //MARK: IMPORT/EXPORT FORM
        const excelIcon = document.createElement("img");
        excelIcon.src = "../img/excel.png";
        excelIcon.style.height = "10px";
        excelIcon.style.marginRight = "5px";

        const exportLink = document.createElement("a");
        exportLink.className = "btn btn-primary my-3";
        exportLink.href = "#";
        exportLink.onclick = doExport;
        exportLink.textContent = "Export";
        exportLink.prepend(excelIcon);
        exportLink.style.flexGrow = "1";

        const importFileField = document.createElement("input");
        importFileField.type = "file";
        importFileField.className = "form-control";
        importFileField.accept = ".xlsx";
        importFileField.onchange = doImport;

        const importFileFieldDiv = document.createElement("div");
        importFileFieldDiv.className = "m-3";
        importFileFieldDiv.style.flexGrow = "1";
        importFileFieldDiv.appendChild(importFileField);

        const siiirSyncCheckbox = document.createElement("input");
        siiirSyncCheckbox.type = "checkbox";
        siiirSyncCheckbox.id = "siiirSync";
        siiirSyncCheckbox.className = "form-check-input";
        siiirSyncCheckbox.name = "siiirSync";
        siiirSyncCheckbox.checked = false;

        const siiirSyncLabel = document.createElement("label");
        siiirSyncLabel.className = "form-check-label";
        siiirSyncLabel.htmlFor = "siiirSync";
        siiirSyncLabel.textContent = "Sincronizare SIIIR";

        const checkboxDiv = document.createElement("div");
        checkboxDiv.className = "form-check m-3";
        checkboxDiv.style.flexGrow = "1";
        checkboxDiv.appendChild(siiirSyncCheckbox);
        checkboxDiv.appendChild(siiirSyncLabel);

        const importForm = document.createElement("form");
        importForm.className = "mx-5";
        importForm.style.display = "flex";
        importForm.style.width = "700px";

        const messageDiv = document.createElement("div");
        messageDiv.id = "message";
        messageDiv.style.flexGrow = "1";
        messageDiv.className = "alert alert-primary";
        messageDiv.innerText = "Alegeți fisierul pentru import";


        importForm.appendChild(exportLink);
        importForm.appendChild(checkboxDiv);
        importForm.appendChild(importFileFieldDiv);
        importForm.appendChild(messageDiv);

        const searchDiv = document.getElementById("search");
        searchDiv.parentNode.insertBefore(importForm, searchDiv);

        //MARK: progress bar outer div
        const progressBarOuterDiv = document.createElement("div");
        progressBarOuterDiv.id = "progressOuter";
        progressBarOuterDiv.className = "progress";
        progressBarOuterDiv.ariaValueNow = "0";
        progressBarOuterDiv.ariaValueMin = "0";
        progressBarOuterDiv.ariaValueMax = "100";
        progressBarOuterDiv.role = "progressbar";
        progressBarOuterDiv.style.width = "98%";
        progressBarOuterDiv.style.height = "20px";
        progressBarOuterDiv.style.display = "none";
        progressBarOuterDiv.style.position = "fixed";
        progressBarOuterDiv.style.top = "1%";
        progressBarOuterDiv.style.left = "1%";

        //MARK: progress bar inner
        const progressBarInnerDiv = document.createElement("div");
        progressBarInnerDiv.id = "progressInner";
        progressBarInnerDiv.className = "progress-bar bg-info";
        progressBarInnerDiv.style.width = "0%";
        progressBarInnerDiv.textContent = "0%";
        //smooth transition
        progressBarOuterDiv.appendChild(progressBarInnerDiv);
        document.body.appendChild(progressBarOuterDiv);

    }

    doSetup();

})();